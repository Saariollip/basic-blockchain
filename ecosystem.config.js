module.exports = {
  apps : [{
    name: "NODE 1",
    script: 'server.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    node_args: "--max_old_space_size=4096",
    max_memory_restart: '2G',
    watch: ["models/"],
    env: {
      NODE_ENV: 'development',
      PORT: 5000
    },
    env_production: {
      NODE_ENV: 'production'
    }
  },
  {
    name: "Node 2",
    script: 'server.js',
    node_args: "--max_old_space_size=4096",
    watch: ["models/"],
    env: {
      PORT: "5001"
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
